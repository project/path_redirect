
(function ($) {

Drupal.behaviors.pathRedirectFieldsetSummaries = {
  attach: function (context) {
    $('fieldset#edit-path-redirect', context).drupalSetSummary(function (context) {
      if ($('table tbody td.empty').size()) {
        return Drupal.t('No redirects');
      }
      else {
        var redirects = $('table tbody tr').size();
        return Drupal.formatPlural(redirects, '1 redirect', '@count redirects');
      }
    });
  }
};

})(jQuery);
